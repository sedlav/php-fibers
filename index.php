<?php

$f1 = new Fiber(function (): string {
	 Fiber::suspend();
	 foreach (range(1, 5) as $v) {
		 Fiber::suspend(sprintf('<span style="background-color: green">Fiber 1: iteration %s</span><br>', $v));
	 }
	 return '';
});

$f2 = new Fiber(function (): string {
	 Fiber::suspend();
	 foreach (range(1, 10) as $v) {
		 Fiber::suspend(sprintf('<span style="background-color: yellow">Fiber 2: iteration %s <br>', $v));
	 }
	 return '';
});

$f3 = new Fiber(function (): string {
	 Fiber::suspend();
	 foreach (range(1, 10) as $v) {
		 Fiber::suspend(sprintf('<span style="background-color: red">Fiber 3: iteration %s <br>', $v));
	 }
	 return '';
});

$fibers = [$f1, $f2, $f3];

while ($fibers) {
	foreach ($fibers as $index => $f) {
		if (!$f->isStarted()) {
			$f->start();
		}
		if ($f->isTerminated()) {
			unset($fibers[$index]);
		} else {
			echo $f->resume();
		}
	}
}
